/* global window, document */
/* eslint-disable func-names, no-magic-numbers, no-var */

/**
 * ECMAScript 5 asset loading placeholder
 * and last resort error handler for
 * - script-loader assets that fail to load
 * - fatal syntax error in a loaded asset
 */
(function () {
  var rows = document
    .getElementById('zs-app')
    .getElementsByTagName('TD')[0]
    .firstChild
    .getElementsByTagName('*');

  rows[0].style.textDecoration = 'none';
  rows[0].style.animation = '3s infinite zs-static-pulse';
  rows[2].style.display = 'none';

  window.onerror = function () {
    rows[0].style.textDecoration = '';
    rows[0].style.animation = '';
    rows[2].style.display = '';
  };
}());
