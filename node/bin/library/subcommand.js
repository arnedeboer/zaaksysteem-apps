const { magenta, yellow } = require('chalk');
const inquirer = require('inquirer');

/**
 * @type {Array}
 */
const choices = [
  {
    name: `${magenta('start')} development`,
    value: 'start',
  },
  {
    name: `${magenta('lint')} the source code`,
    value: 'lint',
  },
  {
    name: `${magenta('test')} the source code`,
    value: 'test',
  },
  {
    name: `${magenta('update')} all dependencies`,
    value: 'update',
  },
  {
    name: `${magenta('add')} a package to a ${yellow('context')}`,
    value: 'add',
  },
  {
    name: `${magenta('remove')} a package from a ${yellow('context')}`,
    value: 'remove',
  },
  {
    name: `${magenta('rtfm')} at http://localhost:8080`,
    value: 'rtfm',
  },
  {
    name: `${magenta('build')} all apps`,
    value: 'build',
  },
  {
    name: `${magenta('spoof')} an @mintlab package`,
    value: 'spoof',
  },
];

const pageSize = 10;

/**
 * @type {Array}
 */
const questions = [
  {
    choices,
    message: 'Available subcommands',
    name: 'command',
    pageSize,
    type: 'list',
  },
];

function subcommand(commands) {
  /**
   * @param {Object} options
   * @param {string} options.command
   */
  function use({ command }) {
    commands[command]();
  }

  inquirer
    .prompt(questions)
    .then(use);
}

module.exports = subcommand;
