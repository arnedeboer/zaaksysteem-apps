const { join } = require('path');
const { sync: globSync } = require('glob');
const hogan = require('hogan.js');
const { minify: minifyHtml } = require('html-minifier');
const { read } = require('./file');
const {
  IS_DEVELOPMENT,
  SERVER_DOCUMENT_ROOT,
} = require('./constants');

/**
 * Get the absolute path of a template asset.
 *
 * @param {string} baseName
 * @return {string}
 */
const asset = baseName =>
  read(join(__dirname, 'template', baseName));

/**
 * @param {string} pattern
 * @return {Array}
 */
const glob = pattern => globSync(pattern, {
  cwd: SERVER_DOCUMENT_ROOT,
});

/**
 * @param {string} name
 * @param {string} extension
 * @return {string}
 */
const getVendorScriptUrl = (name, extension = 'js') => {
  const pattern = `vendor/${name}-*.${extension}`;
  const [vendorScriptUrl] = glob(pattern);

  return `/${vendorScriptUrl}`;
};

/**
 * @param {string} id
 * @param {string} extension
 * @return {*}
 */
const getAppAssetUrl = (id, extension) => {
  const pattern = `${id}/${id}-*.${extension}`;
  const [appAssetUrl] = glob(pattern);

  return `/${appAssetUrl}`;
};

/**
 * @param {string} id
 * @return {string}
 */
const getAppScriptUrl = id => {
  if (IS_DEVELOPMENT) {
    const { PUBLIC_PATH } = require('./development');

    return `${PUBLIC_PATH}${id}.js`;
  }

  return getAppAssetUrl(id, 'js');
};

/**
 * Get the style sheet queue. The webpack dev server dynamically injects
 * style elements, in production style sheets are extracted to a files.
 *
 * @param {string} id
 * @return {string}
 */
const getStyleSheetQueue = id => {
  if (IS_DEVELOPMENT) {
    return [];
  }

  return [getAppAssetUrl(id, 'css')];
};

/**
 * @param array
 * @return {string}
 */
const arrayToStringLiteral = array =>
  `[${array.map(value => `'${value}'`).join()}]`;

/**
 * @param {string} id
 * @return {string}
 */
function getScriptLoader(id) {
  const polyfill = getVendorScriptUrl('ie11');
  const scriptQueue = arrayToStringLiteral([
    getVendorScriptUrl('react'),
    getVendorScriptUrl('ui'),
    getAppScriptUrl(id),
  ]);
  const styleSheetQueue = arrayToStringLiteral(getStyleSheetQueue(id));
  const scriptLoader = asset('script-loader.js')
    .replace('(__POLYFILL__)', polyfill)
    .replace('(__SCRIPT_QUEUE__)', scriptQueue)
    .replace('(__STYLE_SHEET_QUEUE__)', styleSheetQueue);

  return scriptLoader;
}

/**
 * Generate the SPA entry file.
 *
 * @param {Object} options
 * @param {string} options.id
 * @param {string} [options.lang=nl]
 * @param {string} [options.title=Zaaksysteem]
 * @returns {string}
 */
function template({
  id,
  lang = 'nl',
  title = 'Zaaksysteem',
}) {
  const raw = asset('html.mustache');
  const template = hogan.compile(raw);
  const templateData = {
    style: asset('static.css'),
    scriptLoader: getScriptLoader(id),
    failSafe: asset('fail-safe.js'),
    lang,
    title,
  };
  // https://www.npmjs.com/package/html-minifier#options-quick-reference
  const minifierOptions = {
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true,
    removeComments: true,
    removeOptionalTags: true,
  };
  const output = template.render(templateData);

  return minifyHtml(output, minifierOptions);
}

module.exports = template;
