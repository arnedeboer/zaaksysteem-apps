import { runQueue } from '../runQueue';
import { removeFromArray } from '@mintlab/kitchen-sink';

/**
 * Registry for `pushState` subscriptions.
 *
 * @type {Array}
 */
const pushSubscriptions = [];

/**
 * Homegrown pub/sub mechanism for {@link pushState}.
 *
 * @param {Function} callback
 * @return {Function}
 *   Remove the `callback` subscription.
 */
export function onPushState(callback) {
  pushSubscriptions.push(callback);

  return function unsubscribe() {
    removeFromArray(pushSubscriptions, callback);
  };
}

/**
 * Register a handler for the native `popstate` event.
 *
 * @param {Function} callback
 */
export function onPopState(callback) {
  // beware module.hot when using addEventListener
  window.onpopstate = callback;
}

/**
 * Wrap the native `history.pushState` method and run
 * any handlers registered with {@link onPushState}.
 *
 * @param {string} path
 */
export function pushState(path) {
  window.history.pushState(null, '', path);
  runQueue(pushSubscriptions, path);
}

/**
 * Wrap the native `history.replaceState` method.
 *
 * @param {string} path
 */
export function replaceState(path) {
  window.history.replaceState(null, '', path);
}
