import React, { createElement } from 'react';
import { filterStyleSheet } from './Filter.style';
import {
  withStyles,
  Icon,
  Select,
} from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.changeFilterValue
 * @param {Function} props.changeFocus
 * @param {Function} props.removeFocus
 * @param {Boolean} props.hasFocus
 * @param {string} props.name
 * @param {string} props.value
 * @param {string} props.startAdornmentName
 * @param {Object} props.userTranslations
 * @param {Function} props.fetchUsers
 * @param {Array<Object>} props.userOptions
 * @return {ReactElement}
 */
export const TextFieldFilter = ({
  classes,
  changeFilterValue,
  changeFocus,
  removeFocus,
  hasFocus,
  name,
  value,
  startAdornmentName,
  userTranslations,
  fetchUsers,
  userOptions,
}) => (
  <div
    className={classes.filterWrapper}
  >
    <Select
      name={name}
      value={value}
      choices={userOptions}
      generic={true}
      getChoices={fetchUsers}
      hasFocus={hasFocus}
      onChange={changeFilterValue}
      onFocus={changeFocus}
      onBlur={removeFocus}
      translations={userTranslations}
      startAdornment={createElement(Icon, {
        size: 'small',
        children: startAdornmentName,
      })}
      filterOption={option => option}
      isClearable={true}
    />
  </div>
);

export default withStyles(filterStyleSheet)(TextFieldFilter);
