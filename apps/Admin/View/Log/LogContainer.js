import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import actions from '../../Store/Action';
import Log from './Log';
import {
  buildUrl,
  get,
  performGetOnProperties,
} from '@mintlab/kitchen-sink';
import formatRows from './library/formatRows';
import { navigate } from './../../../library/url';

const {
  assign,
  keys,
} = Object;

const INITIAL_COUNT = 0;
const INITIAL_PAGE = 1;
const INITIAL_ROWSPERPAGE = 50;
const PAGE_CORRECTION = 1;

/**
 * @param {string} value
 * @return {Array<number>}
 */
export const numericUiToNumbers = value =>
  value
    .split(/\s+/)
    .map(token => Number(token));

/**
 * @param {Object} data
 * @param {Object} componentTranslations
 * @return {*}
 */
export const getRows = (data, componentTranslations) => {
  const rows = get(data, 'data.rows');

  return rows ? formatRows(rows, componentTranslations) : null;
};

/**
 * @param data
 * @return {*}
 */
export const getCount = data =>
  get(data, 'data.count', INITIAL_COUNT);

/**
 * @param {Object} data
 * @return {*}
 */
export const getUserOptions = data =>
  get(data, 'data', []);

/**
 * @param {Object} data
 * @return {*}
 */
export const getInitialUser = data =>
  get(data, 'data');

/**
 * @param {number} page
 * @param {number} rowsPerPage
 * @param {number} nextRowsPerPage
 * @return {number}
 * Note: This only works when all options of rowsPerPage are multiples of the smallest option
 */
export const getPageByRows = (page, rowsPerPage, nextRowsPerPage) =>
  Math.floor(page * rowsPerPage / nextRowsPerPage);

const paramGetters = {
  caseNumber: 'caseNumber',
  keyword: 'keyword',
  user: 'user.uuid',
};

const paramExternals = {
  caseNumber: 'query:match:case_id',
  keyword: 'query:match:keyword',
  user: 'query:match:subject',
};

/**
 * @param {Object} params
 * @return {Object}
 */
export const createExternalParams = params =>
  keys(params)
    .reduce((acc, param) => {
      acc[paramExternals[param]] = params[param];

      return acc;
    }, {});

/**
 * @param {Object} state
 * @param {Object} state.resource
 * @param {string} state.route
 * @return {Object}
 */
const mapStateToProps = ({
  resource,
  route,
}) => ({
  resource,
  route,
});

/**
 * @param {Function} dispatch
 * @return {Object}
 */
const mapDispatchToProps = dispatch => ({
  request: bindActionCreators(actions.resource.request, dispatch),
  invoke: bindActionCreators(actions.route.invoke, dispatch),
});

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    resource,
    resource: {
      eventlog,
      users,
    },
  } = stateProps;
  const {
    request,
    invoke,
  } = dispatchProps;
  const {
    t,
    segments,
    params = {},
  } = ownProps;

  /**
   * @param {string} name
   * @return {Object}
   */
  const mapColumns = name => ({
    name,
    label: t(`log:column:${name}`),
  });

  const columns = [
    'caseId',
    'date',
    'description',
    'component',
    'user',
  ].map(mapColumns);

  const noResultDescription = t('log:noResultDescription');
  const labelRowsPerPage = t('log:labelRowsPerPage');
  const headerTitle = t('log:title');
  const exportButtonTitle = t('log:exportButtonTitle');
  const keywordTranslation = t('log:filter:placeholder:keyword');
  const caseNumberTranslation = t('log:filter:placeholder:caseNumber');
  const componentTranslations = t('log:components', { returnObjects: true });
  const userTranslations = {
    'form:choose': t('log:filter:placeholder:user'),
    'form:beginTyping': t('log:filter:beginTyping'),
    'form:loading': t('log:filter:loading'),
  };

  const [pageSegement, rowsPerPageSegement] = segments;
  const page = Number(pageSegement || INITIAL_PAGE) - PAGE_CORRECTION;
  const rowsPerPage = Number(rowsPerPageSegement || INITIAL_ROWSPERPAGE);
  const rows = getRows(eventlog, componentTranslations);
  const count = getCount(eventlog);
  const initialUserResourceKey = `subject ${params.user}`;
  const initialUser = getInitialUser(resource[initialUserResourceKey]);
  const userOptions = getUserOptions(users);
  const rowsPerPageOptions = numericUiToNumbers('25 50 100');

  /**
   *   Synthetic event.
   */
  const fetchNewData = () => {
    request(['eventlog', assign(
      {},
      {
        page: page + PAGE_CORRECTION,
        rows_per_page: rowsPerPage,
        'query:match:eventlog': 'bugged',
      },
      createExternalParams(params)
    )]);
  };

  // ZS-TODO: use mapping when it allows dynamic paths
  // ZS-TODO: remove rows param when no longer required
  /**
   *   Synthetic event.
   */
  const fetchUserByUuid = () => {
    request([initialUserResourceKey]);
  };

  /**
   * @param {string} query
   *   Synthetic event.
   */
  const fetchUsers = query => {
    request(['users', {
      query,
      include_admin: '1',
    }]);
  };

  /**
   * @param {Object} route
   * @param {number} route.nextPage
   * @param {number} route.nextRowsPerPage
   * @param {Object} route.nextParams
   *   Synthetic event.
   */
  const invokeRoute = ({
    nextPage = page,
    nextRowsPerPage = rowsPerPage,
    nextParams = params,
  }) => {
    const convertedParams = performGetOnProperties(nextParams, paramGetters);
    const path = buildUrl(`/admin/logboek/${nextPage + PAGE_CORRECTION}/${nextRowsPerPage}`, convertedParams);

    invoke({path});
  };

  /**
   * Download the entire log
   *
   * @param {*} exportParams
   */
  const exportLog = exportParams => {
    const externalParams = createExternalParams(exportParams);
    const url = buildUrl('/api/v1/eventlog/download', externalParams);

    navigate(url);
  };

  return assign({}, stateProps, dispatchProps, ownProps, {
    caseNumberTranslation,
    columns,
    count,
    exportButtonTitle,
    exportLog,
    fetchNewData,
    fetchUserByUuid,
    fetchUsers,
    getPageByRows,
    initialUser,
    invokeRoute,
    keywordTranslation,
    page,
    params,
    rowsPerPage,
    userOptions,
    userTranslations,
    headerTitle,
    labelRowsPerPage,
    noResultDescription,
    rows,
    rowsPerPageOptions,
  });
};

/**
 * Connects {@link Log} with {@link i18next} and the store.
 *
 * @return {ReactElement}
 */
const LogContainer = translate()(connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Log));

export default LogContainer;
