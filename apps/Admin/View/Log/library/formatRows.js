import { createElement } from 'react';
import {
  dictionary,
  get,
} from '@mintlab/kitchen-sink';
import {
  CaseIdCell,
  DateCell,
  ComponentCell,
  UserCell,
} from '../cells';

const { keys } = Object;

const columnDictionary = dictionary({
  caseId: CaseIdCell,
  date: DateCell,
  component: ComponentCell,
  user: UserCell,
});

/**
 * @param {number} key
 * @param {string} value
 * @return {ReactElement}
 */
export const columnFormats = (key, value) => {
  if (columnDictionary[key]) {
    return createElement(columnDictionary[key], {
      value,
    });
  }

  return createElement('span', null, value);
};

/**
 * @param {Array} rows
 * @param {Object} componentTranslations
 * @return {Array}
 */
export const formatRows = (rows, componentTranslations) =>
  rows
    .map(row => {
      const cells = {};

      keys(row)
        .forEach(key => {
          let value = get(row, key);

          if(key === 'component' && componentTranslations) {
            value = get(componentTranslations, value, null);
          }

          cells[key] = columnFormats(key, value);
        });

      return cells;
    });

export default formatRows;
