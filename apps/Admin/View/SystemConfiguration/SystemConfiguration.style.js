/**
 * @return {JSS}
 */
export const systemConfigurationStyleSheet = () => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  grid: {
    display: 'flex',
    height: 'calc(100% - 68px)',
  },
});
