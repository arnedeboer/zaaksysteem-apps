import React, { Component } from 'react';
import { withStyles, Form } from '@mintlab/ui';
import deepMerge from 'deepmerge';
import deepEqual from 'fast-deep-equal';
import validate from '../../../../library/validation/validation';
import { sharedStylesheet } from '../../Shared/Shared.style';
import { formWrapperStylesheet } from './FormWrapper.style';
import { reduceMap } from '@mintlab/kitchen-sink';
import { appToDb } from '../../../Resource/API/transformers';

const { keys } = Object;

/**
 * @param {Function} verify
 * @param {Function} translate
 * @return {Function}
 */
function validateWithTranslation(verify, translate) {
  return (...args) => {
    const error = verify(...args);

    if (error) {
      return translate(`validation:${error}`);
    }
  };
}

/**
 * Renders an @mintlab/ui Form, and hooks into any changes in the
 * errors produced in the form.
 *
 * If any changes have been made but not yet been saved, an
 * action will be dispatched to show a banner with a save button.
 *
 * If the form contains errors, an action will be dispatched to
 * show a banner with an error message.
 *
 * @reactProps {Array} fieldSets
 * @reactProps {Function} set
 * @reactProps {Function} request
 * @reactProps {Function} t
 * @reactProps {Object} banners
 * @reactProps {string} identifier
 * @reactProps {Function} showBanner
 * @reactProps {Function} hideBanner
 * @reactProps {Function} showDialog
 */
export class FormWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: null,
    };

    this.onErrors = this.onErrors.bind(this);
  }

  // Lifecycle methods:

  /**
   * @param {*} prevProps
   * @param {*} prevState
   */
  componentDidUpdate(prevProps, prevState) {
    const {
      props: {
        banners,
        showBanner,
        hideBanner,
        showDialog,
        changed,
        t,
        request,
      },
      state: {
        errors,
      },
    } = this;

    const identifier = 'systemConfigForm';
    const shouldMap = () => (
      !deepEqual(prevProps.changed, changed)
      || !deepEqual(prevState.errors, errors)
    );

    if (!shouldMap()) return;

    const hasErrors = Boolean(errors && keys(errors).length);
    const undo = [{
      action: () =>
        showDialog({
          type: 'DiscardChanges',
        }),
      label: t('form:undo'),
      icon: 'undo',
    }];

    const save = () => {
      const items = changed.reduce((accumulator, field) => {
        const { reference, value } = field;

        accumulator[reference] = {
          value: appToDb(value),
        };
        return accumulator;
      }, {});
      request(['config:save', { items }]);
    };

    const changedBanner = {
      identifier,
      variant: 'secondary',
      label: t('form:unsavedChanges'),
      primary: {
        action: save,
        label: t('form:save'),
        icon: 'close',
      },
      secondary: undo,
    };

    const errorBanner = {
      identifier,
      label: t('form:errorsInForm'),
      variant: 'danger',
      secondary: undo,
    };

    const map = new Map([
      [
        keyHasErrors => keyHasErrors,
        () => showBanner(errorBanner),
      ],
      [
        (keyHasErrors, keyChanged) => Boolean(keyChanged.length),
        () => showBanner(changedBanner),
      ],
      [
        (keyHasErrors, keyChanged, keyBanners) => keyBanners.hasOwnProperty(identifier),
        () => hideBanner({ identifier }),
      ],
    ]);

    reduceMap({
      map,
      keyArguments: [hasErrors, changed, banners],
    });
  }

  render() {
    const {
      fieldSets,
      set,
      identifier,
      classes,
      t,
    } = this.props;

    return (
      <div className={classes.sheet}>
        <Form
          fieldSets={fieldSets}
          set={set}
          validate={validateWithTranslation(validate, t)}
          identifier={identifier}
          classes={classes}
          onErrors={this.onErrors}
        />
      </div>
    );
  }

  // Custom methods:

  onErrors(errors) {
    this.setState({
      errors,
    });
  }
}

/**
 * @param {Object} theme
 * @return {Object}
 */
const mergedStyles = theme =>
  deepMerge(
    sharedStylesheet(theme),
    formWrapperStylesheet(theme)
  );

export default withStyles(mergedStyles)(FormWrapper);
