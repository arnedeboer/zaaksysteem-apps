import React from 'react';
import { Dialog } from '@mintlab/ui';
import { fieldsChanged } from '../../../../library/form';
import { get } from '@mintlab/kitchen-sink';

/**
 * Discard Changes {@link DialogWrapper}.
 *
 * @param {Object} props
 * @param {Object} props.ui
 * @param {Function} props.t
 * @param {*} props.form
 * @param {*} props.resource
 * @param {*} props.set
 * @param {*} props.hide
 * @param {*} props.invoke
 * @return {ReactElement}
 */
const DiscardChanges = ({
  ui: {
    dialog,
  },
  t,
  form,
  resource,
  set,
  hide,
  invoke,
}) => {

  const confirm = () => {
    const { config: { data: { items }}} = resource;
    const path = get(dialog, 'options.path');

    // Reset changed form fields back to the original value, if there is one
    fieldsChanged(form, items).forEach(({ name, originalValue }) => {
      set({ [name]: originalValue });
    });

    hide();

    if (path) {
      invoke({
        path,
        force: true,
      });
    }
  };

  return (
    <Dialog
      open = { true }
      title = { t('dialog:discardChanges:title') }
      text = { t('dialog:discardChanges:text') }
      buttons = {[
        {
          text: t('dialog:ok'),
          onClick: confirm,
          presets: ['primary', 'raised'],
        },
        {
          text: t('dialog:cancel'),
          onClick: hide,
          presets: ['secondary', 'raised'],
        },
      ]}
    />
  );
};

export default DiscardChanges;
