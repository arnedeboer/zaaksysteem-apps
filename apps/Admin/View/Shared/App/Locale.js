import React, { Component } from 'react';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import LoginContainer from './LoginContainer';

const LANGUAGE = 'nl-NL';
const FALLBACK_LANGUAGE = 'en-US';

/**
 * @reactProps {Object} resource
 */
export default class Locale extends Component {
  constructor(props) {
    super(props);
    this.initialize(props.resource.locale.data);
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const {
      resource: {
        navigation,
        session,
      },
    } = this.props;

    return (
      <I18nextProvider i18n={i18n}>
        <LoginContainer
          navigation={navigation.data}
          session={session.data}
        />
      </I18nextProvider>
    );
  }

  /**
   * @param {Object} resources
   */
  initialize(resources) {
    i18n
      .init({
        lng: LANGUAGE,
        fallbackLng: FALLBACK_LANGUAGE,
        resources,
        debug: false,
        react: {
          wait: true,
        },
      });
  }
}
