/**
 * @return {JSS}
 */
export const subAppHeaderStylesheet = () => ({
  header: {
    padding: '0',
    height: '72px',
    display: 'flex',
    'align-items': 'center',
    position: 'relative',
  },
});
