import { InlineFrameLoader } from '../../Shared/InlineFrameLoader';
import { objectifyParams } from '@mintlab/kitchen-sink';
import urlMap from './base';

/**
 * @param {string} url
 * @return {Array}
 */
export default function getComponent(url) {
  const [pathComponent, paramComponent] = url.split('?');
  const [, , segment, ...rest] = pathComponent.split('/');
  const params = objectifyParams(paramComponent);

  if (!urlMap.hasOwnProperty(segment)) {
    return null;
  }

  const value = urlMap[segment];

  // iFrame with legacy src
  if (typeof value === 'string') {
    return [InlineFrameLoader, value];
  }

  // Original React component
  return [value, rest, params];
}
