import actionMan from '../../../library/redux/actionMan';

/**
 * A simplified representation of the store actions.
 * The default export of this module transforms the elements of the
 * array leaves in the tree to action creators that return an object
 * with the signature
 *
 * - `{string}` type
 * - `{*}` payload
 *
 * An action creator yields its associated action type when coerced
 * to a string.
 *
 * @type {Object}}
 */
export const action = {
  auth: [
    'login',
    'logout',
  ],
  form: [
    'set',
  ],
  resource: [
    'abort',
    'request',
    'respond',
  ],
  route: [
    'invoke',
    'resolve',
  ],
  system: [
    'log',
  ],
  ui: {
    banner: [
      'show',
      'hide',
    ],
    dialog: [
      'show',
      'hide',
    ],
    drawer: [
      'open',
      'close',
    ],
    iframe: {
      overlay: [
        'open',
        'close',
      ],
      window: [
        'load',
        'unload',
      ],
    },
    snackbar: [
      'show',
      'hide',
    ],
  },
};

export default actionMan(action);
