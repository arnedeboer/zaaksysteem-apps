import update from 'immutability-helper';
import { createReducer } from '../../../library/redux/createReducer';
import action from '../Action';

const { log } = action.system;
const initialState = [];

/**
 * @type {Function}
 */
export const logReducer = createReducer(initialState, {
  [log]: ({ payload, state }) => update(state, {
    $push: [payload],
  }),
});
