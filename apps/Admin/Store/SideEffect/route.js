import action from '../Action';
import { pushState, replaceState } from '../../../library/dom/history';

const {
  route: {
    invoke,
    resolve,
  },
} = action;

export default {
  /**
   * @param {Object} parameters
   * @param {string} parameters.path
   * @param {boolean|undefined} [parameters.force]
   *   Force route resolving
   * @param {boolean|undefined} [parameters.replace]
   *   Replace the current entry in the history stack
   * @return {Promise<Function>}
   */
  [invoke](parameters) {
    const updateHistory = parameters.replace ? replaceState : pushState;

    updateHistory(parameters.path);

    return Promise.resolve(() => resolve(parameters));
  },
};
