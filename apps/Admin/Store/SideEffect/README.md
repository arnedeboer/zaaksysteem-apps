# Side Effects

> Side effects are handled by a simple Redux middleware: 
dispatch one action to trigger the side effect, have the 
side effect dispatch another action when it is done. Multiple side effects may respond to the same action type.

## Adding a new side effect

1. Create a module that exports an object.
2. Every object property corresponds to an action type
   and is assigned a callback.
3. The callback executes a Promise with a fulfillment
   handler that returns a function that returns another 
   action creator that is automagically dispatched.
4. Import the new side effect in the directory index and 
   add it to the list Array.

## Example

### Promise based async API

    export default function pause(milliseconds = 1000) {
      new Promise(resolve => {
        setTimeout(resolve, milliseconds);
      });
    };

### SideEffect 

    import pause from 'pause';
    import action from '../Action';
    
    const { resume, wait } = action.timeout;
    
    export default {
      // Our action creators yield their action type when 
      // `toString` is called (in this case implicitly).
      [wait]:
        () =>
          pause()
            .then(() =>
              // The middleware dispatches the 
              // return value of this function. 
              () => 
                resume()),
    };
