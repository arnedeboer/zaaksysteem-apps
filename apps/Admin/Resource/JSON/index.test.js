import json from '.';
import { methods } from './fetch';

/**
 * @test {json}
 */
describe('The Resource API module', () => {
  describe('exports an object that', () => {
    test('implements all HTTP methods of the `./json` module', () => {
      const implementedMethodsCount = 2;

      expect.assertions(implementedMethodsCount);

      for (const name of methods) {
        const actual = typeof json[name.toLowerCase()];
        const asserted = 'function';

        expect(actual).toBe(asserted);
      }
    });

    test('throws when you try to reassign a method', () => {
      expect(() => {
        json.get = 42;
      }).toThrow();
    });
  });
});

