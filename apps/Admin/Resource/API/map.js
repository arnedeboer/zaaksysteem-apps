import { dictionary } from '@mintlab/kitchen-sink';

/**
 * Resource ID dictionary for Zaaksysteem API requests.
 *
 * @type {Object}
 */
export const map = dictionary({
  config: {
    method: 'GET',
    url: '/api/v1/config/panel',
  },
  'config:save': {
    method: 'POST',
    url: '/api/v1/config/update',
  },
  eventlog: {
    method: 'GET',
    url: '/api/v1/eventlog',
  },
  session: {
    method: 'GET',
    url: '/api/v1/session/current',
  },
  users: {
    method: 'GET',
    url: '/objectsearch/contact/medewerker',
  },
  subject: {
    method: 'GET',
    url: '/api/v1/subject',
  },
});
